This is an example of how [Infection](https://github.com/infection/infection) mutation testing framework can be integrated with GitLab and its Code Quality feature.

See https://infection.github.io/guide/command-line-options.html#logger-gitlab