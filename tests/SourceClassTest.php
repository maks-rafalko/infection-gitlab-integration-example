<?php

namespace ExampleTest\Test;

use ExampleTest\SourceClass;
use PHPUnit\Framework\TestCase;

class SourceClassTest extends TestCase
{
    public function test_hello()
    {
        $sourceClass = new SourceClass();
        $result = $sourceClass->add(2, 1);

        self::assertGreaterThan(0, $result);
    }
}
